
FROM ubuntu:16.04

WORKDIR /usr/src/app

RUN dpkg --add-architecture i386
RUN apt -y update && apt -y upgrade
RUN apt -y install multiarch-support
RUN apt -y install libc6:i386 libncurses5:i386 libstdc++6:i386 libgcc1:i386 zlib1g:i386
RUN apt -y install socat

RUN groupadd -g 999 chal && useradd -r -u 999 -g chal chal

COPY ./src .

RUN chown -R root:root /usr/src/app/

RUN chmod 444 /usr/src/app/flag.txt
RUN chmod 555 /usr/src/app/leakalicious
RUN chmod 555 /usr/src/app

USER chal

CMD socat TCP-LISTEN:8888,fork,reuseaddr EXEC:"/usr/src/app/leakalicious"
