# leakalicious -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/leakalicious)

## Chal Info

Desc: `King Arthur has proposed this challenge for all those who consider themselves worthy.`

Given files:

* leakalicious

Hints:

* BLUKAT ME! If only we knew what libc to use...

Flag: `TUCTF{cl0udy_w17h_4_ch4nc3_0f_l1bc}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/leakalicious)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/leakalicious:tuctf2019
```
